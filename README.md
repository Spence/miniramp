miniramp
========

SoundCloud artist recommendations, crunching SoundCloud [API](http://developers.soundcloud.com/) data with Python running on GAE webapp2 framework.

App is deployed live [here](http://mini-ramp.appspot.com/).
